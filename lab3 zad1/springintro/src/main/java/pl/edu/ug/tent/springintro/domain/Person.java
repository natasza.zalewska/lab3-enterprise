package pl.edu.ug.tent.springintro.domain;

public class Person {

  private String first_name;
  private String last_name;
  private String email;
  private String company_name;

  public Person() {
    System.out.println("Creating person " + this);
  }

  public Person(String first_name, String last_name, String email, String company_name) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.company_name = company_name;
    System.out.println("Creating person " + this);
  }

  public String getFirstName() {
    return first_name;
  }

  public void setFirstName(String first_name) {
    this.first_name = first_name;
  }

  public String getLastName() {
    return last_name;
  }

  public void setLastName(String last_name) {
    this.last_name = last_name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCompanyName() {
    return company_name;
  }

  public void setCompanyName(String company_name) {
    this.company_name = company_name;
  }


  @Override
  public String toString() {
    return "Person{" +
        "First name ='" + first_name + '\'' +
        ", last name =" + last_name + '\'' +
            ", email =" + email + '\'' +
            ", company name =" + company_name +
        '}';
  }
}
